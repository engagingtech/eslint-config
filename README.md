# @engaging-tech/eslint-config

Standardised [eslint](https://eslint.org/) configuration for all of our front-end codebases.

## Quickstart

1. Install the eslint config: `npm i -D @engaging-tech/eslint-config`
2. Add the following to your `package.json` to enable eslint:

```JSON
{
  "eslintConfig": {
    "extends": [
      "@engaging-tech/eslint-config"
    ]
  }
}
```

3. Add the following extensions to your copy of VSCode to enable in-editor linting and formatting:

- Prettier: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
- ESLint: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

## Usage

Our ESLint config is intended to be a slightly opinionated, yet sensible baseline to ensure a level of code quality. We use [airbnb's eslint config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) as a baseline, with some of our own overrides to change or disable rules that have been more annoying than useful for us.

These rules are intended to be used in _all_ of our front-end projects - because of this we have some react rules enabled by default, which might mean these rules aren't as valuable when used on back-end code.

## Contributing

All of our eslint rules can be found in `index.js`.

To learn more about ESLint's rules, check [their documentation](https://eslint.org/docs/rules/).

To learn more about how to modify the structure of our ESLint config, check the [official documentation](https://eslint.org/docs/user-guide/configuring).

**Note:** If you are adding or removing rules, please make **very sure** that this repo's `package.json` is up-to-date. Breakages here may break linting **EVERYWHERE**.
